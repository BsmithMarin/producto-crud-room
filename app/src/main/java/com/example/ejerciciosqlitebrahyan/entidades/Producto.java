package com.example.ejerciciosqlitebrahyan.entidades;

import android.content.ContentValues;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.MapInfo;
import androidx.room.PrimaryKey;

import com.example.ejerciciosqlitebrahyan.bbdd.ProductContract;

import java.util.Objects;

@Entity(tableName = ProductContract.ProductoEntry.TABLE_NAME,
        indices = {@Index(value = {ProductContract.ProductoEntry.COLUMN_NAME_DESCRIPCION},
                unique = true),
                @Index(value = ProductContract.ProductoEntry.COLUMN_NAME_PRECIO,unique = true)} )
public class Producto {

    @ColumnInfo(name = ProductContract.ProductoEntry.COLUMN_NAME_DESCRIPCION)
    @NonNull
    private String descripcion;

    @PrimaryKey
    @ColumnInfo(name = ProductContract.ProductoEntry.COLUMN_NAME_CODIGO)
    private int codigo;

    @ColumnInfo(name = ProductContract.ProductoEntry.COLUMN_NAME_PRECIO)
    @NonNull
    private double precio;

    public Producto(int codigo, String descripcion, double precio){
        setCodigo(codigo);
        setDescripcion(descripcion);
        setPrecio(precio);
    }

    public ContentValues toContentValues(){

        ContentValues values = new ContentValues();
        values.put(ProductContract.ProductoEntry.COLUMN_NAME_CODIGO,getCodigo());
        values.put(ProductContract.ProductoEntry.COLUMN_NAME_DESCRIPCION,getDescripcion());
        values.put(ProductContract.ProductoEntry.COLUMN_NAME_PRECIO,getPrecio());
        return values;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int condigo) {
        this.codigo = condigo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return codigo == producto.codigo && Double.compare(producto.precio, precio) == 0 && Objects.equals(descripcion, producto.descripcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descripcion, codigo, precio);
    }

    @Override
    public String toString() {
        return "Producto{" +
                "descripcion='" + descripcion + '\'' +
                ", condigo=" + codigo +
                ", precio=" + precio +
                '}';
    }
}
