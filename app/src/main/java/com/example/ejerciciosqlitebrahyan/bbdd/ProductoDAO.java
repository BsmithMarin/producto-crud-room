package com.example.ejerciciosqlitebrahyan.bbdd;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.ejerciciosqlitebrahyan.entidades.Producto;

import java.util.List;

@Dao
public interface ProductoDAO {

    @Query("SELECT * FROM "+ProductContract.ProductoEntry.TABLE_NAME)
    List<Producto> getAll();

    @Query("SELECT * FROM "+ProductContract.ProductoEntry.TABLE_NAME +" WHERE "+
    ProductContract.ProductoEntry.COLUMN_NAME_DESCRIPCION +" LIKE :nombre")
    Producto getByName(String nombre);

    @Query("SELECT * FROM "+ProductContract.ProductoEntry.TABLE_NAME+" WHERE "+
    ProductContract.ProductoEntry.COLUMN_NAME_CODIGO+" = :codigo")
    Producto getByCodigo(int codigo);

    @Query("DELETE FROM "+ProductContract.ProductoEntry.TABLE_NAME+" WHERE "+
    ProductContract.ProductoEntry.COLUMN_NAME_CODIGO+" = :codigo")
    void deleteByCodigo(int codigo);

    @Insert
    void insert(Producto producto);

    @Delete
    void delete(Producto producto);

    @Update
    void update(Producto producto);
}
