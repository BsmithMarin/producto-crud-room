package com.example.ejerciciosqlitebrahyan.util;

import android.content.Context;

import androidx.room.Room;

import com.example.ejerciciosqlitebrahyan.bbdd.AppDatabase;
import com.example.ejerciciosqlitebrahyan.bbdd.ProductContract;

/**
 * Sigue el patron Singleton, para solo exista una instancia de la base de datos
 * la cual es costosa de crear
 */
public class RoomDataBaseUtil {

    private static AppDatabase db;

    private RoomDataBaseUtil(){}

    public static AppDatabase getInstance(Context context){
        if(db == null){
            db = Room.databaseBuilder(context, AppDatabase.class, ProductContract.DATA_BASE_NAME).build();
        }
        return db;
    }

}
